import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;

  minLong = 5;

  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nameValidator,
        this.nameValidatorParam(this.minLong)
      ])],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      //console.log('form change: ' form)
    });
  }

  ngOnInit() {
  }

  guardar(nombre: string, url: string): boolean{
    const d =  new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nameValidator(control: FormControl): {[s: string]: boolean}{
    let l =  control.value.toString().trim().lenght;
    if(l < 0 && l < 5){
      return { invlalidName: true }
    }
    return null;
  }

  nameValidator(control: FormControl): {[s: string]: boolean}{
    const l =  control.value.toString().trim().length;
    if(l > 0 && l < 5){
      return { invalidName: true }
    }
    return null;
  }

  nameValidatorParam(minLong: number): ValidatorFn{
    return (control: FormControl):  {[s: string]: boolean} | null => {
      const l =  control.value.toString().trim().length;
      if(l > 0 && l < minLong){
        return { tooShortName: true }
      }
      return null;
    }
  }



}
